import string
import nltk
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from pymystem3 import Mystem
import tkinter as tk


class App:
    def __init__(self, root):
        self.classificator = Classificator('learn.csv')
        self.text_field = tk.Entry(root)
        self.text_field.pack()
        self.text_field.focus_set()

        self.b = tk.Button(root, text="Определить класс", width=20, command=self.button_click)
        self.b.pack()
        self.listbox = tk.Listbox(root, height=10, width=80)
        self.listbox.pack()

    def handler_work_out(self, res, str_text, path):
        data = pd.read_csv(path, names=['Class', 'Description'])
        res_arr = []
        res_arr.append(str(str_text).upper() + '\n')
        for i in range(len(res)):
            tmp = []
            for j in range(len(res[i])):
                tmp.append(round(float(res[i][j]), 2))
        tmp_res = {}
        for i in range(len(tmp)):
            if tmp[i] != 0.0:
                tmp_res[tmp[i]]=i
        for i in sorted(tmp_res.keys(), reverse=True):
            res_arr.append('Относится к классу ' + data['Description'][tmp_res[i]+1] + ' (' + str((tmp_res[i]+1)) +
                           ')' + ' с вероятностью ' + str(i) + '\n')
        print('Классификация рабочей выборки завершена!')
        return res_arr

    def button_click(self):
        print(self.text_field.get())
        str_text = self.text_field.get()
        res = self.classificator.work_prediction(str_text)
        res_arr = self.handler_work_out(res, str_text, 'class.csv')
        self.listbox.delete(0, tk.END)
        for index, res_arr in enumerate(res_arr):
            self.listbox.insert(index, res_arr)
        self.scr = tk.Scrollbar(root, command=self.listbox.yview)
        self.listbox.configure(yscrollcommand=self.scr.set)


class Classificator:
    def __init__(self, csv_path):
        # Формирую обучающую быборку
        data, real_data_number = self.get_data(csv_path)
        m = Mystem()
        temp_tokens = self.get_tokens(data)
        text = ' '.join(temp_tokens)
        self.tokens = set(m.lemmatize(text))
        token_dict = self.get_token_dict(list(data['Questions']))
        # tfs - матрица, строки - предложения выборки, столбцы - все слова из выборки,
        # на пересечении - tfidf каждого слово в каждом предложении, т.е. важность слова
        tfidf = TfidfVectorizer(sublinear_tf=True, max_df=0.5, analyzer='word', vocabulary=list(self.tokens),
                                ngram_range=(1, 3))
        tfs = tfidf.fit_transform(token_dict.values())
        y = list(data['Number'])
        x = tfs.toarray()
        self.rf_model = RandomForestClassifier(n_estimators=20)
        self.rf_model.fit(x, y)

    # считаваение из входного файла
    def get_data(self, csv_path):
        data = pd.read_csv(csv_path, names=['Number', 'Questions', 'Answer 1', 'Answer 2'])
        for val in range(len(data)):
            if str(data['Answer 1'][val]) == 'nan':
                data['Answer 1'][val] = ''
            if str(data['Answer 2'][val]) == 'nan':
                data['Answer 2'][val] = ''
            data['Questions'][val] = str(data['Questions'][val]) + ' ' + str(data['Answer 1'][val]) + ' ' + str(
                data['Answer 2'][val])
        del data['Answer 1']
        del data['Answer 2']
        data = data[1:]
        data['Questions'] = data.Questions.str.lower()
        data['Number'] = (data['Number'].astype(int)) * (-1)
        real_data_number = set(data['Number'])

        for idx, val in enumerate(set(data['Number'])):
            for j in range(1, len(data) + 1):
                if int(data['Number'][j]) == val:
                    data.loc[j, 'Number'] = idx + 1
        return data, real_data_number

    # удаление всех знаков пунктуации в строке
    def del_punctuation(self, text):
        return text.translate(str.maketrans('', '', string.punctuation))

    # получает все слова из выбоки
    def get_tokens(self, data):
        no_punctuation = []
        for text in data['Questions'].values:
            no_punctuation.append(self.del_punctuation(text))
        tokens_list = []
        for i in no_punctuation:
            tokens_list += nltk.word_tokenize(i)
        tokens = list(set(tokens_list))
        return tokens

    # создание словаря, соответсвие слов предложения из выборки
    # (нужен для подсчета tfidf)
    def get_token_dict(self, arr):
        m = Mystem()
        text_data = ''
        lines = len(arr)
        for line in range(lines):
            text = arr[line]
            no_punctuation = ''.join(self.del_punctuation(text))
            text_data += '!!!!' + no_punctuation
        lemmes = ' '.join(m.lemmatize(text_data)).split('!!!!')[1:]
        lines = range(lines)
        token_dict = dict(zip(lines, lemmes))
        return token_dict

    def work_prediction(self, str_text):
        # return probability array

        m = Mystem()
        text = str(self.del_punctuation(str_text))
        lemmers = m.lemmatize(text)[:-1]
        list_lemmers = list(filter(lambda x: x != ' ', lemmers))
        str_lemmers = ' '.join(list_lemmers)
        token_dict = {}
        token_dict[0] = str_lemmers

        tfidf_test = TfidfVectorizer(sublinear_tf=True,
                                     max_df=0.5,
                                     analyzer='word',
                                     vocabulary=list(self.tokens),
                                     ngram_range=(1, 3))
        tfs_test = tfidf_test.fit_transform(token_dict.values())
        x_test = tfs_test.toarray()

        # res - итоговая матрица, строки - предложения из тестовой выбоки, столбцы - классы,
        # на пересечении - вероятность пренадлежности предложения к данному классу
        res = self.rf_model.predict_proba(x_test)
        return res

    def test_prediction(self, test_path, path_result):
        # Формирую тестовую выборку
        data_test, real_data_number = self.get_data(test_path)
        token_dict_test = self.get_token_dict(list(data_test['Questions']))
        tfidf_test = TfidfVectorizer(sublinear_tf=True,
                                     max_df=0.5,
                                     analyzer='word',
                                     vocabulary=list(self.tokens),
                                     ngram_range=(1, 3))
        tfs_test = tfidf_test.fit_transform(token_dict_test.values())
        Y_test = list(data_test['Number'])
        # Y_test = list(real_data_number)
        X_test = tfs_test.toarray()
        # res - итоговая матрица, строки - предложения из тестовой выбоки, столбцы - классы,
        # на пересечении - вероятность пренадлежности предложения к данному классу
        res = self.rf_model.predict_proba(X_test)
        columns = ['Строка', 'Предполагаемый класс', 'Вероятность', 'Реальный класс', 'Ошибка',
                   'Список возможных вероятностей']
        df = pd.DataFrame(columns=columns, )
        y_val = []
        for i in range(len(res)):
            tmp = []
            for j in range(len(res[i])):
                tmp.append(round(float(res[i][j]), 2))
            class_index = tmp.index(max(tmp)) + 1
            is_correct = True if class_index == Y_test[i] else False
            y_val.append(is_correct)
            tmp = list(filter(lambda x: x != 0.0, tmp))
            tmp.sort()
            tmp.reverse()
            df.loc[len(df)] = [data_test['Questions'][i + 1], class_index, tmp[0], Y_test[i], is_correct, tmp]
        df.to_csv(path_result, encoding='utf-8')
        print('Классификация тестовой выборки завершена!')


if __name__ == '__main__':
    root = tk.Tk()
    app = App(root)
    # classificator = Classificator('learn.csv')
    # classificator.test_prediction('test.csv', 'result_test.csv')
    root.mainloop()
