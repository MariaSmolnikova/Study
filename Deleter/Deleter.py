import io
import os
import sys

print("DEFAULT_BUFFER_SIZE = ", io.DEFAULT_BUFFER_SIZE)


def secure_delete(path, passes, use_rnd):
    '''
    Функция удаления файла. BUFFER_SIZE - размер буфера, обнуляю его и передаю
    функции для открытия файла
    :param path: путь до файла
    :param passes: количество затираний
    :param use_rnd: использовать рандомные символы для затирания или нет
    :return: None
    '''
    BUFFER_SIZE = 0
    with open(path, "ba+", BUFFER_SIZE) as delfile:
        length = delfile.tell()
        for i in range(passes):
            if use_rnd:
                delfile.write(os.urandom(length))
            else:
                delfile.write(bytearray(length))
            delfile.flush()
    os.remove(path)


def secure_delete_folder(path, passes, use_rnd):
    '''
    Функция удаления папки
    :param path: путь до файла
    :param passes: количество затираний
    :param use_rnd: использовать рандомные символы для затирания или нет
    :return:
    '''
    for f in os.listdir(path):
        full_name = os.path.join(path, f)
        secure_delete(full_name, passes, use_rnd)


if __name__ == '__main__':
    if len(sys.argv) == 3:
        path = sys.argv[0]
        passes = int(sys.argv[1])
        use_rnd = bool(sys.argv[2])
        if os.path.isdir(path):
            secure_delete_folder(path, passes, use_rnd)
        elif os.path.isfile(path):
            secure_delete(path, passes, use_rnd)
        else:
            print("Incorrect name! It is not folder or file")
    else:
        print('Неверное число аргументов')

