import matplotlib.pyplot as plt
import Matrix

def plot(matrix):
    height = len(matrix.array_matr_transponse[30])
    for i in range(31):
        for j in range(31):
            xF = []
            yF = []
            xG = []
            yG = []
            xU = []
            yU = []
            if i < j:
                if matrix.is_number_vector(i) == False:
                    x = matrix.get_vector(i)
                else:
                    x = matrix.array_matr_transponse[i]

                if matrix.is_number_vector(j) == False:
                    y = matrix.get_vector(j)
                else:
                    y = matrix.array_matr_transponse[j]

                for arg in range(height):
                    if str(matrix.array_matr_transponse[30][arg])[:1] == 'F':
                        xF.append(x[arg])
                        yF.append(y[arg])
                    if str(matrix.array_matr_transponse[30][arg])[:1] == 'G':
                        xG.append(x[arg])
                        yG.append(y[arg])
                    if str(matrix.array_matr_transponse[30][arg])[:1] == 'U':
                        xU.append(x[arg])
                        yU.append(y[arg])

                plt.xlabel(str(matrix.name[i])[2:][:-1])
                plt.ylabel(str(matrix.name[j])[2:][:-1])
                plt.scatter(xF, yF, color='red')
                plt.scatter(xG, yG, color='blue')
                plt.scatter(xU, yU, color='green')
                plt.legend(("F","G","U"))
                plt.savefig(str(matrix.name[i])[2:][:-1] + '-' + str(matrix.name[j])[2:][:-1] + '.png')
                plt.cla()
                plt.clf()
                print 'step', i, j



def main():
    ma = Matrix('test.csv')
    plot(ma)
if __name__ == "__main__":
    main()


