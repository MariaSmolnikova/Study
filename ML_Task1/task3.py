import matplotlib.pyplot as plt
import numpy as np
import Matrix

def get_static_value(matrix, path, decision_rule = 0.5):
    tn = tp = fn = fp = 0.0
    result_tn = result_fp = result_tp = result_fn = 0.0
    height = len(matrix.array_matr_transponse[30])

    for index in range(height):
        if float(matrix.array_matr_transponse[path][index]) >= decision_rule:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                tp += 1
            else:
                fp += 1
        if float(matrix.array_matr_transponse[path][index]) < decision_rule:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                fn += 1
            else:
                tn += 1
    if tn == 0 and fp == 0:
        result_tn = 0.0
        result_fp = 0.0
    else:
        result_tn = tn / (tn + fp)
        result_fp = fp / (tn + fp)
    if tp == 0 and fn == 0:
        result_tp = 0.0
        result_fn = 0.0
    else:
        result_tp = tp / (tp + fn)
        result_fn = fn / (tp + fn)
    return round(result_tn, 3), round(result_fp, 3), round(result_tp, 3), round(result_fn, 3)


def get_plot(matrix, path):
    decision_rule = 0.01
    y = []
    x = []
    while decision_rule < 1:
        a, b, c, d = get_static_value(matrix, path, decision_rule)
        x.append(b)
        y.append(c)
        decision_rule += 0.01
    plt.plot(x, y,)
    plt.plot(x, x)
    plt.savefig('ROC-' + str(matrix.name[path])[2:][:-1] + '.png')

    gini = 0
    for index in range(len(x)):
        if y[index] - x[index] > gini:
            gini = y[index] - x[index]
    print 'Gini coefficient in ' + str(matrix.name[path])[2:][:-1] + '=' + str(gini)
    plt.cla()
    plt.clf()


def main():
    ma = Matrix('test.csv')
    for index in range(25, 30):
        get_plot(ma, index)

if __name__ == "__main__":
    main()
