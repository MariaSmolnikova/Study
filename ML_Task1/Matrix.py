class Matrix:
    def __init__(self, path):
        self.matr = []
        with open(path) as fin:
            line1 = fin.readlines()
            for line in line1:
                self.matr.append(list(line.split(';')))
        self.name = str(self.matr[:1])[2:][:-6].split(',')
        self.matr = self.matr[1:]
        self.array_matr_transponse = map(list, zip(*self.matr))
        self.height = len(self.array_matr_transponse[30])

    def is_number(self, str_val):
        try:
            float(str_val)
            return True
        except ValueError:
            return False

    def is_number_vector(self, index):
        for arg in range(self.height):
            if not self.is_number(self.array_matr_transponse[index][arg]):
                return False
        return True

    def get_dict(self, curr_list):
        d = dict()
        k = 0
        for val in curr_list:
            if not(val in d):
                d[val] = k
                k += 1
        return d

    def get_vector(self, index):
        new_x = []
        curr_dict = self.get_dict(self.array_matr_transponse[index])
        for arg in self.array_matr_transponse[index]:
            new_x.append(curr_dict[arg])
        return new_x