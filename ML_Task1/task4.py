import matplotlib.pyplot as plt
import Matrix


def get_static_value(matrix, decision_rule=0.5):
    tn = tp = fn = fp = 0.0
    result_tn = result_fp = result_tp = result_fn = 0.0
    height = len(matrix.array_matr_transponse[30])

    for index in range(height):
        voteCounterF = 0
        voteCounterG = 0
        for arg in range(25, 30, 1):
            if float(matrix.array_matr_transponse[arg][index]) >= decision_rule:
                voteCounterF += 1
            if float(matrix.array_matr_transponse[arg][index]) < decision_rule:
                voteCounterG += 1
        if voteCounterF >= 3:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                tp += 1
            else:
                fp += 1
        else:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                fn += 1
            else:
                tn += 1

        if voteCounterG >= 3:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                fn += 1
            else:
                tn += 1
        else:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                tp += 1
            else:
                fp += 1

    if tn == 0 and fp == 0:
        result_tn = 0.0
        result_fp = 0.0
    else:
        result_tn = tn / (tn + fp)
        result_fp = fp / (tn + fp)
    if tp == 0 and fn == 0:
        result_tp = 0.0
        result_fn = 0.0
    else:
        result_tp = tp / (tp + fn)
        result_fn = fn / (tp + fn)
    return round(result_tn, 8), round(result_fp, 8), round(result_tp, 8), round(result_fn, 8)


def get_fp(matrix):
    decision_rule = 0.1
    probability_fp = 1
    while 1:
        a, b, c, d = get_static_value(matrix, decision_rule=decision_rule)
        if b < probability_fp:
            decision_rule += 0.01
            probability_fp = b
        else:
            return decision_rule


def get_static_value_with_div(matrix, decision_rule=0.5):
    tn = tp = fn = fp = 0.0
    result_tn = result_fp = result_tp = result_fn = 0.0
    height = len(matrix.array_matr_transponse[30])

    for index in range(height):
        tmp = 0
        for arg in range(25, 28, 1):
            tmp += float(matrix.array_matr_transponse[arg][index])
        tmp /= 3
        if tmp >= decision_rule:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                tp += 1
            else:
                fp += 1
        if tmp < decision_rule:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                fn += 1
            else:
                tn += 1
    if tn == 0 and fp == 0:
        result_tn = 0.0
        result_fp = 0.0
    else:
        result_tn = tn / (tn + fp)
        result_fp = fp / (tn + fp)
    if tp == 0 and fn == 0:
        result_tp = 0.0
        result_fn = 0.0
    else:
        result_tp = tp / (tp + fn)
        result_fn = fn / (tp + fn)
    return round(result_tn, 3), round(result_fp, 3), round(result_tp, 3), round(result_fn, 3)


def get_plot(matrix):
    decision_rule = 0.01
    y = []
    x = []
    while decision_rule < 1:
        a, b, c, d = get_static_value_with_div(matrix, decision_rule)
        x.append(b)
        y.append(c)
        decision_rule += 0.01
    plt.plot(x, y,)
    plt.plot(x, x)
    plt.savefig('ROC.png')


def main():
    ma = Matrix('test.csv')
    f = open('task4.txt', 'w')
    f.write('Step > 0.5' + '\n' + 'tn, fp, tp, fn =' + str(get_static_value(ma, decision_rule=0.5)) + '\n')
    f.write('Step > 0.8' + '\n' + 'tn, fp, tp, fn =' + str(get_static_value(ma, decision_rule=0.8)) + '\n')
    f.write('min false positive, step = ' + str(get_fp(ma)) + '\n')
    get_plot(ma)
    f.close()
if __name__ == "__main__":
    main()