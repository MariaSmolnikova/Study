import matplotlib.pyplot as plt
import Matrix

def get_static_value(matrix):
    step = 0.05
    height = len(matrix.array_matr_transponse[30])
    gini = []
    param = []
    a_param = b_param = c_param = 0.1
    while a_param < 1:
        b_param = 0.1
        while b_param < 1:
            c_param = 0.1
            while c_param < 1:
                y = []
                x = []
                decision_rule = 0.1
                while decision_rule < 1:
                    tn = tp = fn = fp = 0.0
                    for index in range(height):
                        tmp = a_param*float(matrix.array_matr_transponse[25][index]) + \
                              b_param * float(matrix.array_matr_transponse[26][index]) +\
                              c_param * float(matrix.array_matr_transponse[27][index])
                        tmp /= (a_param + b_param + c_param)
                        if tmp >= decision_rule:
                            if matrix.array_matr_transponse[30][index][:1] == 'F':
                                tp += 1
                            else:
                                fp += 1
                        if tmp < decision_rule:
                            if matrix.array_matr_transponse[30][index][:1] == 'F':
                                fn += 1
                            else:
                                tn += 1

                    if tn == 0 and fp == 0:
                        result_fp = 0.0
                    else:
                        result_fp = float(fp) / (tn + fp)
                    if tp == 0 and fn == 0:
                        result_tp = 0.0
                    else:
                        result_tp = float(tp) / (tp + fn)

                    x.append(result_fp)
                    y.append(result_tp)
                    decision_rule += step
                tmp_gini = 0
                for index in range(len(x)):
                    if y[index] - x[index] > tmp_gini:
                        tmp_gini = y[index] - x[index]
                gini.append(tmp_gini)
                param.append([a_param, b_param, c_param])
                #print 'A=', A_param, 'B=', B_param, 'C=', C_param, 'Gini koeff = ', round(gini[len(gini) - 1], 3)
                c_param += step
            b_param += step
        a_param += step
    return param[gini.index(min(gini))], min(gini), param[gini.index(max(gini))], max(gini)


def get_max_tp(matrix):
    step = 0.1
    height = len(matrix.array_matr_transponse[30])
    param = []
    a_param = b_param = c_param = 0.1
    y = []
    x = []
    while a_param < 1:
        b_param = 0.1
        while b_param < 1:
            c_param = 0.1
            while c_param < 1:
                decision_rule = 0.1
                while decision_rule < 1:
                    tn = tp = fn = fp = 0.0
                    for index in range(height):
                        tmp = a_param*float(matrix.array_matr_transponse[25][index]) + \
                              b_param * float(matrix.array_matr_transponse[26][index]) + \
                              c_param * float(matrix.array_matr_transponse[27][index])
                        tmp /= (a_param + b_param + c_param)
                        if tmp >= decision_rule:
                            if matrix.array_matr_transponse[30][index][:1] == 'F':
                                tp += 1
                            else:
                                fp += 1
                        if tmp < decision_rule:
                            if matrix.array_matr_transponse[30][index][:1] == 'F':
                                fn += 1
                            else:
                                tn += 1

                    if tn == 0 and fp == 0:
                        result_fp = 0.0
                    else:
                        result_fp = float(fp) / (tn + fp)
                    if tp == 0 and fn == 0:
                        result_tp = 0.0
                    else:
                        result_tp = float(tp) / (tp + fn)

                    x.append(round(result_fp, 1))
                    y.append(result_tp)

                    param.append([round(a_param, 2), round(b_param, 2), round(c_param, 2), round(decision_rule, 2)])
                    decision_rule += step

                c_param += step
            b_param += step
        a_param += step

    mass_tp = []
    for index in range(len(x)):
        if x[index] == 0.1:
            mass_tp.append(y[index])
            # print massTp[len(massTp) - 1]
    return param[y.index(max(mass_tp))], round(max(mass_tp), 2)


def main():
    ma = Matrix('test.csv')
    print get_static_value(ma)
    print get_max_tp(ma)
if __name__ == "__main__":
    main()
