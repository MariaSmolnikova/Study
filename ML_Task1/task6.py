import time
import datetime
import matplotlib.pyplot as plt
import Matrix


def get_timestamp(str_val):
    return time.mktime(datetime.datetime.strptime(str_val, "%d.%m.%Y %H:%M").timetuple())


def get_timestamp_struct(matrix, index):
    str_cell = matrix.array_matr_transponse[0][index]
    lib_timestamp = get_timestamp(str_cell)
    return datetime.datetime.fromtimestamp(lib_timestamp)


def calculate_data_hash(timestamp_struct):
    return timestamp_struct.year * 365 + timestamp_struct.month * 12 + timestamp_struct.day


def get_dictionary(matrix):
    d = {}
    for i in range(matrix.height):
        timestamp = get_timestamp_struct(matrix, i)
        data_hash = calculate_data_hash(timestamp)
        if data_hash not in d.keys():
            d[data_hash] = []
        d[data_hash].append(i)
    return d


def get_static_value(matrix, path, decision_rule = 0.5):
    tn = tp = fn = fp = 0.0
    day_dict = get_dictionary(matrix)

    x_recall = []
    x_precision = []
    for arg in day_dict:
        for index in day_dict[arg]:
            if float(matrix.array_matr_transponse[path][index]) >= decision_rule:
                if matrix.array_matr_transponse[30][index][:1] == 'F':
                    tp += 1
                else:
                    fp += 1
            if float(matrix.array_matr_transponse[path][index]) < decision_rule:
                if matrix.array_matr_transponse[30][index][:1] == 'F':
                    fn += 1
                else:
                    tn += 1

        if tn == 0 and fp == 0:
            result_tn = 0.0
            result_fp = 0.0
        else:
            result_tn = round(tn / (tn + fp), 5)
            result_fp = round(fp / (tn + fp), 5)
        if tp == 0 and fn == 0:
            result_tp = 0.0
            result_fn = 0.0
        else:
            result_tp = round(tp / (tp + fn), 5)
            result_fn = round(fn / (tp + fn), 5)

        recall = result_tp / (result_tp + result_fn)
        precision = result_tp / (result_tp + result_fp)

        x_recall.append(recall)
        x_precision.append(precision)
    return x_precision, x_recall


def main():
    ma = Matrix('test.csv')
    day_dict = get_dictionary(ma)
    for path in range(25, 28):
        x1, x2 = get_static_value(ma, path=path)
        plt.scatter(x1, day_dict.keys())
    plt.savefig('Precision.png')

    for path in range(25, 28):
        plt.scatter(x2, day_dict.keys())
    plt.savefig('Recall.png')
if __name__ == "__main__":
    main()
