import Matrix


def get_static_value(matrix, path, decision_rule = 0.5):
    tn = tp = fn = fp = 0.0
    result_tn = result_fp = result_tp = result_fn = 0.0
    height = len(matrix.array_matr_transponse[30])

    for index in range(height):
        if float(matrix.array_matr_transponse[path][index]) >= decision_rule:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                tp += 1
            else:
                fp += 1
        if float(matrix.array_matr_transponse[path][index]) < decision_rule:
            if matrix.array_matr_transponse[30][index][:1] == 'F':
                fn += 1
            else:
                tn += 1
    if tn == 0 and fp == 0:
        result_tn = 0.0
        result_fp = 0.0
    else:
        result_tn = tn / (tn + fp)
        result_fp = fp / (tn + fp)
    if tp == 0 and fn == 0:
        result_tp = 0.0
        result_fn = 0.0
    else:
        result_tp = tp / (tp + fn)
        result_fn = fn / (tp + fn)
    return round(result_tn, 3), round(result_fp, 3), round(result_tp, 3), round(result_fn, 3)


def get_decision_rule(matrix, path):
    probability_fp = 0.2
    decision_rule = 0.01
    while 1:
        a, b, c, d = get_static_value(matrix, path, decision_rule)
        if b > probability_fp:
            decision_rule += 0.01
        else:
            return decision_rule


def get_recall_precision(matrix, path):
    tn, fp, tp, fn = get_static_value(matrix, path)
    recall = tp/(tp + fn)
    precision = tp/(tp + fp)
    return round(recall, 3), round(precision, 3)


def main():
    ma = Matrix('test.csv')
    f = open('task2.txt', 'w')
    for index in range(25, 30):
        f.write(str(ma.name[index])[2:][:-1] + ':\n')
        f.write('tn, fp, tp, fn ' + str(get_static_value(ma, index)) + '\n')
        f.write('decisionRule = ' + str(get_decision_rule(ma, index)) + '\n')
        f.write('recall, precision ' + str(get_recall_precision(ma, index)) + '\n\n')
    f.close()

if __name__ == "__main__":
    main()