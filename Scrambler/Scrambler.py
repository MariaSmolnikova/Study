from collections import Counter


def scramble(name):
    tmp_dictionary = Counter(name)
    dictionary = dict(zip(tmp_dictionary.values(),tmp_dictionary.keys()))
    val = sorted(list(dictionary.keys()), reverse=True)
    res = ''
    for i in val:
        for j in range(i):
            res += dictionary[i]
    return res

