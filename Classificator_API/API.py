from flask import Flask, request
import Classificator_API.Classificator as Classificator
app = Flask('flaskwp1')
classificator = None


@app.route("/",  methods=['GET'])
def hello():
    return open('API.html', 'rt', encoding="utf-8").read()


@app.route("/codebase/webix.css")
def data_codebase_1():
    return open('codebase/webix.css', 'rt').read()


@app.route("/codebase/webix.js")
def data_codebase_2():
    return open('codebase/webix.js', 'rt').read()


@app.route("/resolve", methods=['GET'])
def data_main():
    str_for_class = request.args['title']
    v1 = classificator.work_prediction(str_for_class)
    result = classificator.handler_work_out(res=v1, str_text=str_for_class, path='C:/Users/Maria/Documents/Python Scripts/Classificator_API/class.csv')
    return result

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return "404 error"


if __name__ == '__main__':
    classificator = Classificator.Classificator('C:/Users/Maria/Documents/Python Scripts/Classificator_API/learn.csv')
    app.run()
